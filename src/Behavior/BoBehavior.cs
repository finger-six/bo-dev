namespace Boem
{
	using Godot;

	using System.Collections.Generic;

	using static FollowCamera2D;
	using static InputMacro;

	public enum BoState
	{
		IDLE,
		RUN,
		JUMP,
		FALL,
		ROLL,
		DUCK
	}

	public static class BoStateExt
	{
		public static string GetString (this BoState state)
		{
			switch (state)
			{
				case BoState.IDLE:  { return "IDLE"; }
				case BoState.RUN:   { return "RUN"; }
				case BoState.JUMP:  { return "JUMP"; }
				case BoState.FALL:  { return "FALL"; }
				case BoState.ROLL:  { return "ROLL"; }
				case BoState.DUCK:  { return "DUCK"; }
				default:            { return "N/A"; }
			}
		}

		public static string __ (this BoState state) 	{ return state.GetString(); }
	}

	public class BoBehavior : Behavior
	{
		new private BoPawn                      self;

		#region Constructor
		public BoBehavior (BoPawn self) : base (self,new Dictionary<string,State>())
		{
			this.self=(self as BoPawn);

			this.states.Add(BoState.IDLE.__(),new State(IDLE_START,IDLE_INPUT,IDLE_TICK,IDLE_PTICK,IDLE_STOP));
			this.states.Add(BoState.RUN.__(),new State(RUN_START,IDLE_INPUT,RUN_TICK,RUN_PTICK,RUN_STOP));
			this.states.Add(BoState.JUMP.__(),new State(JUMP_START,AIR_INPUT,AIR_TICK,AIR_PTICK,JUMP_STOP));
			this.states.Add(BoState.FALL.__(),new State(FALL_START,AIR_INPUT,AIR_TICK,AIR_PTICK,FALL_STOP));
			this.states.Add(BoState.ROLL.__(),new State(ROLL_START,ROLL_INPUT,ROLL_TICK,ROLL_PTICK,ROLL_STOP));
		}
		#endregion

		#region IDLE state
		void IDLE_START ()
		{
			self.ResetJumps();
			self.snap_vector=Vector2.Down*50f;
			self.player.Play(BoState.IDLE.__());
			self.camera.SetTargets(CAMERA_ZOOM_IDLE,CAMERA_SMOOTH_DEFAULT,Vector2.Zero);
		}

		void IDLE_INPUT (InputEvent _)
		{
			if (_.IsActionPressed(INPUT_JUMP)&&self.CanJump())
			{
				self.long_jump=false;
				self.fsm.SetCurrentState(BoState.JUMP.__());
				return;
			}

			if (_.IsActionPressed(INPUT_ROLL))
			{
				self.fsm.SetCurrentState(BoState.ROLL.__());
				return;
			}
		}

		void IDLE_TICK (float dt)
		{
			self.CheckWhereFacing();
			self.FlipSprite(!self.facing_right);

			self.audio_player.Tick(dt,false);

			self.camera.Tick(dt);
		}

		void IDLE_PTICK (float dt)
		{
			if (Input.IsActionPressed(INPUT_ML)||Input.IsActionPressed(INPUT_MR))
			{
				self.fsm.SetCurrentState(BoState.RUN.__());
				return;
			}

			if (null==self.GetFloorTerrain())
			{
				self.fsm.SetCurrentState(BoState.FALL.__());
				return;
			}

			if (self.run_accelerator>Mathf.Abs(self.velocity_delta.x))
				self.velocity_delta.x=0;
			else
				self.velocity_delta.x-=self.run_accelerator*self.GetFloorTerrain().ground_friction*(self.IsMovingRight()?2f:-2f);
			
			self.velocity_delta.y=self.gravity;

			self.MoveAndSlideWithSnap(self.velocity_delta,self.snap_vector,Vector2.Up,true);
		}

		void IDLE_STOP (string to)
		{
			self.camera.Freeze();
		}
		#endregion

		#region RUN state
		void RUN_START ()
		{
			self.ResetJumps();
			self.player.Play(BoState.RUN.__());
			self.camera.SetTargets(CAMERA_ZOOM_MOVE,CAMERA_SMOOTH_TIGHT,Vector2.Zero);
		}

		void RUN_TICK (float dt)
		{
			self.CheckWhereFacing();
			self.FlipSprite(!self.facing_right);

			self.audio_player.Tick(dt,true);

			self.camera.Tick(dt);
		}

		void RUN_PTICK (float dt)
		{
			if (null==self.GetFloorTerrain()||!self.ShapeOnFloor())
			{
				self.fsm.SetCurrentState(BoState.FALL.__());
				return;
			}

			sbyte direction=0;
			if (Input.IsActionPressed(INPUT_ML))
				--direction;
			if (Input.IsActionPressed(INPUT_MR))
				++direction;

			if (0==direction)
			{
				self.fsm.SetCurrentState(BoState.IDLE.__());
				return;
			}

			float delta=direction*self.run_accelerator;
			self.velocity_delta.x+=delta;
			self.ClampVelocity(ref self.velocity_delta.x,self.max_run_velocity);

			self.velocity_delta.y=self.gravity;

			self.MoveAndSlideWithSnap(self.velocity_delta,self.snap_vector,Vector2.Up,true);

			self.camera.SetOffsetTarget((float)direction,0f);
		}

		void RUN_STOP (string to)
		{
			self.camera.Freeze();
		}
		#endregion

		#region JUMP/FALL state
		void JUMP_START ()
		{
			self.Jump();
			self.snap_vector=Vector2.Zero;
			self.raycast.Enabled=false;
			self.player.Play(BoState.JUMP.__());
			self.camera.SetTargets(CAMERA_ZOOM_FAST,CAMERA_SMOOTH_TIGHT,Vector2.Up);
		}

		void FALL_START ()
		{
			self.raycast.Enabled=false;
			self.player.Play(BoState.FALL.__());
			self.camera.SetTargets(CAMERA_ZOOM_FAST,CAMERA_SMOOTH_SUPERTIGHT,Vector2.Up);
		}

		void AIR_INPUT (InputEvent _)
		{
			if (_.IsActionPressed(INPUT_JUMP)&&self.CanJump())
				self.Jump();
		}

		void AIR_TICK (float dt)
		{
			if (0f<self.velocity_delta.y&&self.fsm.IsCurrentState(BoState.JUMP.__()))
				self.fsm.SetCurrentState(BoState.FALL.__());

			self.CheckWhereFacing();
			self.FlipSprite(!self.facing_right);

			self.audio_player.Tick(dt,false);

			self.camera.Tick(dt);
		}

		void AIR_PTICK (float dt)
		{
			if (null!=self.GetFloorTerrain()||(!self.jumped&&self.ShapeOnFloor()))
			{
				self.fsm.SetCurrentState((self.IsMovingHoriz()?BoState.RUN:BoState.IDLE).__());
				return;
			}
			
			sbyte direction=0;
			if (Input.IsActionPressed(INPUT_ML))
				--direction;
			if (Input.IsActionPressed(INPUT_MR))
				++direction;

			if (0!=direction)
			{
				float delta=direction*self.run_accelerator;
				self.velocity_delta.x+=delta;
				if (!self.long_jump)
					self.ClampVelocity(ref self.velocity_delta.x,self.max_run_velocity);
			}
			else
				self.velocity_delta.x=0f;

			self.velocity_delta.y+=self.gravity;
			self.ClampVelocity(ref self.velocity_delta.y,self.jump_velocity);

			self.MoveAndSlideWithSnap(self.velocity_delta,self.snap_vector,Vector2.Up,true);
			self.jumped=false;

			self.camera.SetOffsetTarget((float)direction,-1f);
		}

		void JUMP_STOP (string to)
		{
			self.camera.Freeze();
		}

		void FALL_STOP (string to)
		{
			self.snap_vector=Vector2.Down*50f;
			self.raycast.Enabled=true;
			self.velocity_delta.y=0f;
			self.long_jump=self.jumped=false;
			self.camera.Freeze();
		}
		#endregion

		#region ROLL state
		void ROLL_START ()
		{
			self.player.Play(BoState.ROLL.__());
			self.camera.SetTargets(CAMERA_ZOOM_FAST,CAMERA_SMOOTH_SUPERTIGHT,Vector2.Zero);
		}

		void ROLL_INPUT (InputEvent _)
		{
			if (_.IsActionPressed(INPUT_ROLL))
			{
				self.fsm.SetCurrentState((self.IsMovingHoriz()?BoState.RUN:BoState.IDLE).__());
				return;
			}

			if (_.IsActionPressed(INPUT_JUMP)&&self.CanJump())
			{
				self.long_jump=true;
				self.fsm.SetCurrentState(BoState.JUMP.__());
				return;
			}
		}

		void ROLL_TICK (float dt)
		{
			if (self.angle_changed)
				self.sprite.Rotation-=self.current_angle;

			if (self.IsMovingHoriz()&&!self.IsOnWall())
			{
				float intensity=self.velocity_delta.x/self.max_roll_velocity;
				self.sprite.Rotation+=intensity*Mathf.Pi/30f;
			}

			self.audio_player.Tick(dt,false);

			self.camera.Tick(dt);
		}

		void ROLL_PTICK (float dt)
		{
			sbyte direction=0;
			if (Input.IsActionPressed(INPUT_ML))
				--direction;
			if (Input.IsActionPressed(INPUT_MR))
				++direction;
				
			float 	delta=direction*self.roll_accelerator,
					floor_angle=self.GetFloorAngle(),
				  	slope_accel=0f;
			if (self.ShapeOnFloor()&&null!=self.GetFloorTerrain())
			{
				// NOTE(jam):	The accelerator might be a bit too strong? It's making it hard for Bo to go up 30 degree slopes.
				//				Either tweak the random physics values, or only use this at half strength?
				slope_accel=self.gravity*Mathf.Sin(floor_angle)-(self.GetFloorTerrain().ground_friction/(100f*self.mass));
				if (0f>self.GetFloorNormal().x)
					slope_accel*=-1f;

				self.velocity_delta.y=self.gravity;
			}
			else
			{
				self.velocity_delta.y+=self.gravity;
				self.ClampVelocity(ref self.velocity_delta.y,self.jump_velocity);
			}

			if (0==direction&&self.roll_accelerator*10f<Mathf.Abs(self.velocity_delta.x))	// help with rapid direction changes
				self.velocity_delta*=0.66667f;
			
			self.velocity_delta.x+=slope_accel+delta;
			if (floor_angle<Mathf.Pi*0.02778f&&0==direction)	// if floor angle is less than 1 degree and not moving
			{
				self.velocity_delta.x-=self.roll_accelerator*((0f>slope_accel)?-0.16667f:0.16667f);
				if (self.roll_accelerator>=Mathf.Abs(self.velocity_delta.x))
					self.velocity_delta.x=0f;
				// TODO(jam): He slows to a stop on flat floors properly now, but he still gets caught on walls strangely - fix!
			}
			self.ClampVelocity(ref self.velocity_delta.x,self.max_roll_velocity);

			Vector2 testvec=(self.position_current-self.position_previous)+(Vector2.Down*self.gravity/Engine.IterationsPerSecond);
			self.snap_vector=self.TestMove(self.Transform,testvec)?Vector2.Down*50f:Vector2.Zero;
			self.MoveAndSlideWithSnap(self.velocity_delta,self.snap_vector,Vector2.Up,false);

			self.camera.SetOffsetTarget((float)direction,0f);
		}

		void ROLL_STOP (string to)
		{
			self.snap_vector=Vector2.Zero;
			self.sprite.Rotation=0f;
			self.camera.Freeze();
		}
		#endregion
	}
}
