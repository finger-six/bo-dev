namespace Boem
{
    public static class AudioBusMacro
    {
        #region Main buses
        public static string    BUS_SFX=                    "SFX",
                                BUS_MUSIC=                  "MUSIC",
                                BUS_VOICE=                  "VOICE"
                                ;
        #endregion

        #region SFX buses
        public static string    BUS_SFX_FOOTSTEP=           "SFX_fstep",
                                BUS_SFX_PLAYER_FOOTSTEP=    "SFX_pfstep"
                                ;
        #endregion
    }
}