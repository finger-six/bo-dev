namespace Boem
{
    using Godot;

    using static AudioBusMacro;
    using static ResourceMacro;

    public class PawnAudioPlayer : Node2D
    {
        #region Godot nodes
        private AudioStreamPlayer       footstep_player;
        #endregion

        #region Member variables
        private Pawn                    self;

        private float                   footstep_accum;
        #endregion

        #region Editor-accessed variables
        [Export] readonly bool          has_footstep=           true;

        [Export] readonly float         footstep_frequency=     0.5f;
        #endregion

        #region Initialization functions
        public void Init (Pawn self)
        {
            this.self=self;

            this.footstep_player=       this.GetNode   ("fstep_player")    as AudioStreamPlayer;
            this.footstep_player.Bus=   (self.is_player)?BUS_SFX_PLAYER_FOOTSTEP:BUS_SFX_FOOTSTEP;
            this.footstep_accum=0.8f*this.footstep_frequency;
        }
        #endregion

        #region Tick functions
        public void SetFootstep (float dt)
        {
            this.footstep_accum+=dt;
            if (this.footstep_accum>=this.footstep_frequency&&null!=self.GetFloorTerrain())
            {
                this.footstep_accum-=this.footstep_frequency;

                string fstep_type=self.GetFloorTerrain().footstep_sfx.ToLower();
                string fstep_path=RES_SFX+"footstep/footstep_"+fstep_type+"_00"+GD.Randi()%SFX_FSTEP_COUNT+".ogg";
                AudioStreamOGGVorbis fstep_stream=GD.Load(fstep_path) as AudioStreamOGGVorbis;
                
                this.footstep_player.Stream=fstep_stream;
            }
        }

        public void Tick (float dt,bool play_footstep=false)
        {
            if (this.has_footstep&&play_footstep)
            {
                this.SetFootstep(dt);
                if (!this.footstep_player.Playing)
                    this.footstep_player.Play();
            }
        }
        #endregion
    }
}