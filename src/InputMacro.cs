namespace Boem
{
    public static class InputMacro
    {
        public static string    INPUT_JUMP=         "bo_jump",
                                INPUT_ROLL=         "bo_roll",
                                INPUT_TALK=         "bo_talk",
                                INPUT_MOVE_LEFT=    "bo_move_left",
                                INPUT_MOVE_RIGHT=   "bo_move_right",
                                INPUT_ML=           INPUT_MOVE_LEFT,
                                INPUT_MR=           INPUT_MOVE_RIGHT
                                ;
    }
}