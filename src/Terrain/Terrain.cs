namespace Boem
{
    using Godot;

    public class Terrain : StaticBody2D
    {
        #region Member variables
        private Polygon2D               render_polygon=     null;
        #endregion

        #region Editor-accessed variables
        [Export] public float           ground_friction=    1.0f;
        [Export] private readonly bool  draw_platforms=     true;
        [Export(PropertyHint.ColorNoAlpha)]
        private readonly Color          platform_color=     new Color(1.0f,1.0f,1.0f,1.0f);
        [Export(PropertyHint.Enum,"GRASS,SNOW,CONCRETE,CARPET,WOOD")]
        public readonly string          footstep_sfx=       "GRASS";
        #endregion

        #region Initialization functions
        public override void _Ready ()
        {
            if (this.draw_platforms)
                foreach (Node node in this.GetChildren())
                    if (node is CollisionPolygon2D)
                    {
                        CollisionPolygon2D  poly=node as CollisionPolygon2D;
                        Polygon2D           draw=new Polygon2D();

                        draw.Color=this.platform_color;
                        draw.Polygon=poly.Polygon;
                        draw.Position=poly.Position;
                        draw.Rotation=poly.Rotation;
                        draw.Scale=poly.Scale;

                        this.AddChild(draw);
                        this.render_polygon=draw;
                    }
        }
        #endregion
    }
}