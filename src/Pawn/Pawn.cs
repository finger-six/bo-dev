namespace Boem
{
	using Godot;

	using System.Collections.Generic;

	public abstract class Pawn : KinematicBody2D
	{
		#region Member variables
			#region Godot nodes
			public AnimationPlayer              player;
			public Sprite                       sprite;
			protected List<CollisionShape2D>    shapes;
			public RayCast2D                    raycast;
			protected Timer                     coyote_timer,
												buffer_timer;
			#endregion

			#region State management
			public StateMachine                 fsm;
			#endregion

			#region Movement
			public Vector2                      velocity_delta,
												snap_vector,
												position_current,
												position_previous;
			public int                          jumps_left;
			public bool                         jumped,
												long_jump;
			#endregion

			#region Rotation management
			public float                        current_angle;
			public bool                         angle_changed,
												facing_right;
			#endregion

			#region Audio
			public PawnAudioPlayer        		audio_player;
			#endregion

			#region Misc. flags
			public bool							is_player=				false;
			#endregion
		#endregion

		#region Editor-accessed variables
			#region Movement
			[Export] public readonly float      mass;
			[Export] public readonly float      max_run_velocity;
			[Export] public readonly float      max_roll_velocity;
			[Export] public readonly float      run_accelerator;
			[Export] public readonly float      roll_accelerator;
			[Export] protected readonly int     jump_count;
			[Export] public readonly float      jump_velocity;
			[Export] public readonly float      gravity;
			#endregion

			#region Timers
			[Export] protected readonly float   coyote_time;
			[Export] protected readonly float   buffer_time;
			#endregion
		#endregion

		#region Initialization functions
		public override void _Ready ()
		{
			// Load Godot nodes
			this.audio_player=	this.GetNode	("audio")		as PawnAudioPlayer;
			this.player=        this.GetNode    <AnimationPlayer>   ("player");
			this.sprite=        this.GetNode    <Sprite>            ("sprite");
			this.raycast=       this.GetNode    <RayCast2D>         ("raycast");
			this.coyote_timer=  this.GetNode    <Timer>             ("coyote_timer");
			this.buffer_timer=  this.GetNode    <Timer>             ("buffer_timer");
			this.shapes=        new List        <CollisionShape2D>  ();
			foreach (Node node in this.GetChildren())
				if (node is CollisionShape2D)
					this.shapes.Add(node as CollisionShape2D);

			this.audio_player.Init(this);
		}
		#endregion

		#region Tick functions
		private void CheckRotation ()
		{
			float angle_buffer=this.current_angle;
			this.current_angle=(this.IsOnFloor())?this.GetFloorNormal().Angle()+Mathf.Pi/2:0f;
			this.angle_changed=(angle_buffer!=this.current_angle);
			this.Rotation=this.current_angle;
		}

		private void OrientRaycast (RayCast2D ray)
		{
			if (ray.Enabled)
				ray.CastTo=ray.ToLocal(ray.ToGlobal(ray.Position)+(Vector2.Down*1000f)).Normalized()*50f;
		}

		public override void _Process (float dt)
		{
			this.fsm.TickCurrent(dt);
		}

		public override void _PhysicsProcess (float dt)
		{
			this.position_previous=this.position_current;
			this.position_current=this.Position;
			this.fsm.PhysTickCurrent(dt);
			this.CheckRotation();
			this.OrientRaycast(this.raycast);
		}
		#endregion

		#region Physics functions
		public bool ShapeOnFloor () { return base.IsOnFloor(); }
		new public bool IsOnFloor () { return base.IsOnFloor()&&this.CheckFloor(); }
		public bool IsMovingHoriz () { return 0f!=this.velocity_delta.x; }
		public bool IsMovingRight () { return 0f<this.velocity_delta.x; }
		public bool CanJump () { return 0<this.jumps_left; }

		public bool CheckFloor () { return this.raycast.Enabled&&this.raycast.IsColliding(); }
		public Terrain GetFloorTerrain () { return (this.CheckFloor())?this.raycast.GetCollider() as Terrain:null; }
		new public Vector2 GetFloorNormal () { return (this.CheckFloor())?this.raycast.GetCollisionNormal():Vector2.Zero; }

		public void ResetJumps ()
		{
			this.long_jump=this.jumped=false;
			this.jumps_left=this.jump_count;
		}

		public void Jump ()
		{
			if (this.long_jump)
			{
				// Unlikely anyone other than Bo is going to enter this branch
				// but whatever, this really won't be expensive enough to matter

				// TODO(jam): Amplify velocity_delta.x linearly(?) with some fraction of current vd.x * jump_velocity
				
				float	dir=!this.IsMovingRight()?-1f:1f,
						lj_x=(this.jump_velocity*0.1f)+((this.jump_velocity*0.75f*Mathf.Abs(this.velocity_delta.x))/this.max_roll_velocity);

				this.velocity_delta.x=dir*lj_x;
				this.velocity_delta.y=-this.jump_velocity;    // 3/4 of normal jump height - tweak?
			}
			else
				this.velocity_delta.y=-this.jump_velocity;

			--this.jumps_left;
			this.jumped=this.coyote_timer.Paused=this.buffer_timer.Paused=true;
		}

		public void ClampVelocity (ref float vel,float clamp)
		{
			if (clamp<Mathf.Abs(vel))
				vel=Mathf.Clamp(vel,-clamp,clamp);
		}
		#endregion

		#region Render functions
		public void CheckWhereFacing ()
		{
			if (this.IsMovingHoriz())
				this.facing_right=this.IsMovingRight();
		}

		public void FlipSprite (bool flip_h,bool flip_v=false)
		{
			this.sprite.FlipH=flip_h;
			this.sprite.FlipV=flip_v;
		}
		#endregion

		#region Signals
		private void OnCoyoteTimer ()
		{
			this.coyote_timer.Paused=true;
			--this.jumps_left;
		}

		private void OnBufferTimer ()
		{
			this.buffer_timer.Paused=true;
		}
		#endregion
	}
}
