namespace Boem
{
    using Godot;

    public abstract class PlayerPawn : Pawn
    {
        #region Editor-accessed variables
            #region Godot nodes
            public FollowCamera2D               camera;
            #endregion
        #endregion

        #region Initialization functions
        public override void _Ready ()
        {
            this.camera=    this.GetNode    <FollowCamera2D>    ("camera");
            base._Ready();
            this.is_player= true;
        }
        #endregion

        #region Input functions
        public override void _Input (InputEvent evt)
        {
            this.fsm.InputCurrent(evt);
        }
        #endregion
    }
}