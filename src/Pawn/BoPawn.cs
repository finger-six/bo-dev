namespace Boem
{
    public class BoPawn : PlayerPawn
    {
        #region Initialization functions
        public override void _Ready ()
        {
            base._Ready();
            this.fsm=new StateMachine(new BoBehavior(this),BoState.IDLE.GetString());
        }
        #endregion
    }
}