namespace Boem
{
    public static class ResourceMacro
    {
        public static string    RES_ROOT=           "res://res_temp/",
                                RES_SFX=            RES_ROOT+"sfx/"
                                                    ;

        public static int       SFX_FSTEP_COUNT=    5
                                                    ;
    }
}