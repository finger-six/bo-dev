namespace Boem
{
    using Godot;

    using System;
    using System.Collections.Generic;

    public class StateMachine
    {
        #region Member variables
        private Behavior                    behavior;
        public KeyValuePair <string,State>  current_state;
        private bool                        empty;
        #endregion

        #region Constructor
        public StateMachine (Behavior behavior,string def_state=null)
        {
            this.behavior=behavior;
            this.empty=(0==behavior.states.Count);
            if (null!=def_state&&this.behavior.states.TryGetValue(def_state,out State state))
                this.current_state=new KeyValuePair<string,State>(def_state,state);
        }
        #endregion

        #region State execution functions
        public void TickCurrent (float dt)
        {
            if (!this.empty)
                this.current_state.Value._Tick(dt);
        }
        
        public void PhysTickCurrent (float dt)
        {
            if (!this.empty)
                this.current_state.Value._PhysTick(dt);
        }

        public void InputCurrent (InputEvent evt)
        {
            if (!this.empty&&null!=this.current_state.Value._Input)
                this.current_state.Value._Input(evt);
        }
        #endregion

        #region State management functions
        public bool IsCurrentState (string key)
        {
            return String.Equals(this.current_state.Key,key);
        }

        public void SetCurrentState (string key)
        {
            if (this.IsCurrentState(key))
                return;

            if (this.behavior.states.TryGetValue(key,out State state))
            {
                if (null!=this.current_state.Value)
                    this.current_state.Value._Stop(key);

                this.current_state=new KeyValuePair<string,State>(key,state);
                this.current_state.Value._Start();
            }
        }

        public void AddState (string key,State state)
        {
            this.behavior.states.Add(key,state);
            this.empty=false;
        }

        public void RemoveState (string key,State state)
        {
            if (this.IsCurrentState(key))
                foreach (string name in this.behavior.states.Keys)
                    if (!key.Equals(name))
                    {
                        this.SetCurrentState(name);
                        break;
                    }

            this.behavior.states.Remove(key);
            if (0==this.behavior.states.Count)
                this.empty=true;
        }
        #endregion
    }
}