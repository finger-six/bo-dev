namespace Boem
{
    using System.Collections.Generic;

    public abstract class Behavior
    {
        public Dictionary   <string,State>  states;
        protected Pawn                      self;

        public Behavior (Pawn self,Dictionary <string,State> states)
        {
            this.self=self;
            this.states=states;
        }
    }
}