namespace Boem
{
    using Godot;

    public delegate void    _Del_v      ();
    public delegate void    _Del_f      (float f);
    public delegate void    _Del_s      (string s);
    public delegate void    _Del_e      (InputEvent e);

    public class State
    {
        #region Member variables
        public _Del_v       _Start;
        public _Del_s       _Stop;
        public _Del_f       _Tick,
                            _PhysTick;
        public _Del_e       _Input;
        #endregion

        #region Constructor
        public State
        (
            _Del_v  start,
            _Del_f  tick,
            _Del_f  ptick,
            _Del_s  stop
        ) : this (start,null,tick,ptick,stop)
        {}

        public State
        (
            _Del_v  start,
            _Del_e  input,
            _Del_f  tick,
            _Del_f  ptick,
            _Del_s  stop
        )
        {
            this._Start=start;
            this._Input=input;
            this._Tick=tick;
            this._PhysTick=ptick;
            this._Stop=stop;
        }
        #endregion
    }
}