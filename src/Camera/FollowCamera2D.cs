namespace Boem
{
    using Godot;

    public class FollowCamera2D : Camera2D
    {
        #region Member variables
            #region Zoom
            private float                   zoom_start,
                                            zoom_target,
                                            zoom_current,
                                            zoom_lerp_accum;
            #endregion

            #region Smoothing
            private float                   smooth_start,
                                            smooth_target,
                                            smooth_current,
                                            smooth_lerp_accum;
            #endregion

            #region Offset
            private Vector2                 offset_start,
                                            offset_target,
                                            offset_current;
            private float                   offset_lerp_accum;
            #endregion
        #endregion

        #region Editor-accessed variables
        // NOTE(jam): I would have liked to have make these an enum - it'd make adding new lerp types cleaner
        //              But ultimately I don't think it's worth putzing around with the Godot export attribute extensions
        [Export(PropertyHint.Enum,"LINEAR,QUADRATIC,CUBIC,EXPONENTIAL,ELASTIC")]
        private readonly string             zoom_lerp_type,
                                            smooth_lerp_type,
                                            offset_lerp_type;

        [Export] private readonly float     zoom_lerp_time,
                                            smooth_lerp_time,
                                            offset_lerp_time,
                                            offset_multiplier;
        #endregion

        #region Constants
            #region Zoom
            public static float             CAMERA_ZOOM_IDLE=           0.8f,
                                            CAMERA_ZOOM_MOVE=           1.0f,
                                            CAMERA_ZOOM_FAST=           1.1f;
            #endregion

            #region Smoothing
            public static float             CAMERA_SMOOTH_DEFAULT=      9f,
                                            CAMERA_SMOOTH_TIGHT=        11f,
                                            CAMERA_SMOOTH_SUPERTIGHT=   20f;
            #endregion
        #endregion

        #region Initialization functions
        public override void _Ready ()
        {
            this.zoom_start=        this.zoom_target=       this.zoom_current=      CAMERA_ZOOM_IDLE;
            this.smooth_start=      this.smooth_target=     this.smooth_current=    CAMERA_SMOOTH_DEFAULT;
            this.offset_start=      this.offset_target=     this.offset_current=    Vector2.Zero;
            this.zoom_lerp_accum=   this.smooth_lerp_accum= this.offset_lerp_accum= 0f;
        }
        #endregion

        #region Tick functions
        public void ToggleCurrent ()
        {
            this.Current^=true;
        }

            #region Lerp calculations
            private static float LinearLerp (float delta) { return delta; }

            public static float QuadLerp (float delta)
            {
                if ((delta*=2f)<1f)
                    return 0.5f*delta*delta;
                return -0.5f*((delta-=1f)*(delta-2f)-1f);
            }

            private static float CubicLerp (float delta)
            {
                if ((delta*=2f)<1f)
                    return 0.5f*delta*delta*delta;
                return 0.5f*((delta-=2f)*delta*delta+2f);
            }

            private static float ExpLerp (float delta)
            {
                if (0f==delta)
                    return 0f;
                if (1f==delta)
                    return 1f;
                if (1f>(delta*=2f))
                    return 0.5f*Mathf.Pow(1024f,delta-1f);
                return 0.5f*(-Mathf.Pow(2f,-10f*(delta-1f))+2f);
            }

            private static float ElasticLerp (float delta)
            {
                if ((delta*=2f)<1f)
                    return -0.5f*Mathf.Pow(2f,10*(delta-=1f))*Mathf.Sin((delta-0.1f)*(2f*Mathf.Pi)/0.4f);
                return Mathf.Pow(2f,-10f*(delta-=1f))*Mathf.Sin((delta-0.1f)*(2f*Mathf.Pi)/0.4f)*0.5f+1f;
            }
            #endregion

        private void LerpZoom (float dt)
        {
            this.zoom_lerp_accum+=dt;
            float   pct=Mathf.Clamp(this.zoom_lerp_accum/this.zoom_lerp_time,0,1),
                    dz;

            switch (this.zoom_lerp_type)
            {
                case "QUADRATIC":   {   dz=QuadLerp(pct);       }   break;
                case "CUBIC":       {   dz=CubicLerp(pct);      }   break;
                case "EXPONENTIAL": {   dz=ExpLerp(pct);        }   break;
                case "ELASTIC":     {   dz=ElasticLerp(pct);    }   break;
                default:            {   dz=LinearLerp(pct);     }   break;
            }

            if (1f<=pct)
                this.zoom_lerp_accum=0f;
            
            this.zoom_current=this.zoom_start+dz*(this.zoom_target-this.zoom_start);
            this.Zoom=new Vector2(this.zoom_current,this.zoom_current);
        }

        private void LerpSmooth (float dt)
        {
            this.smooth_lerp_accum+=dt;
            float   pct=Mathf.Clamp(this.smooth_lerp_accum/this.smooth_lerp_time,0,1),
                    ds;

            switch (this.zoom_lerp_type)
            {
                case "QUADRATIC":   {   ds=QuadLerp(pct);       }   break;
                case "CUBIC":       {   ds=CubicLerp(pct);      }   break;
                case "EXPONENTIAL": {   ds=ExpLerp(pct);        }   break;
                case "ELASTIC":     {   ds=ElasticLerp(pct);    }   break;
                default:            {   ds=LinearLerp(pct);     }   break;
            }

            if (1f<=pct)
                this.smooth_lerp_accum=0f;

            this.smooth_current=this.smooth_start+ds*(this.smooth_target-this.smooth_start);
            this.SmoothingSpeed=this.smooth_current;
        }

        private void LerpOffset (float dt)
        {
            this.offset_lerp_accum+=dt;
            float   pct=Mathf.Clamp(this.offset_lerp_accum/this.offset_lerp_time,0,1),
                    df;

            switch (this.zoom_lerp_type)
            {
                case "QUADRATIC":   {   df=QuadLerp(pct);       }   break;
                case "CUBIC":       {   df=CubicLerp(pct);      }   break;
                case "EXPONENTIAL": {   df=ExpLerp(pct);        }   break;
                case "ELASTIC":     {   df=ElasticLerp(pct);    }   break;
                default:            {   df=LinearLerp(pct);     }   break;
            }

            if (1f<=pct)
                this.offset_lerp_accum=0f;

            this.offset_current=this.offset_start+df*(this.offset_target-this.offset_start);
            this.Offset=this.offset_current;
        }

        public void Tick (float dt)
        {
            if (this.Zoom.y!=this.zoom_target)
                this.LerpZoom(dt);

            if (this.SmoothingSpeed!=this.smooth_target)
                this.LerpSmooth(dt);

            if (this.Offset!=this.offset_target)
                this.LerpOffset(dt);
        }

        public void SetZoomTarget       (float target)      { this.zoom_target=target; }
        public void SetSmoothingTarget  (float target)      { this.smooth_target=target; }
        public void SetOffsetTarget     (Vector2 target)    { this.offset_target=target*this.offset_multiplier; }
        public void SetOffsetTarget     (float target_x,
                                         float target_y)    { this.offset_target=new Vector2(target_x,target_y)*this.offset_multiplier; }
        public void SetTargets          (float? zoom=null,float? smooth=null,Vector2? offset=null)
        {
            if (zoom is float zoom_target)
                this.SetZoomTarget(zoom_target);

            if (smooth is float smooth_target)
                this.SetSmoothingTarget(smooth_target);

            if (offset is Vector2 offset_target)
                this.SetOffsetTarget(offset_target);
        }

        public void Freeze ()
        {
            this.zoom_start=this.Zoom.y;
            this.smooth_start=this.SmoothingSpeed;
            this.offset_start=this.Offset;

            this.zoom_lerp_accum=this.smooth_lerp_accum=this.offset_lerp_accum=0f;
        }
        #endregion
    }
}