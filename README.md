# Bo & Em: Space Rocks! Development Repo
_Bo & Em: Space Rocks!_ is a 2D platformer, currently in development by _jamspoon_ and _bat.zack.brandinelli_.

This repository is used as our development archive. It's important, though, to at least make this repo publicly available so it can be helpful as a learning resource. For now, this will pose no issues, as there are no finalized art assets / levels / etc - though when we reach that stage, we will likely make this repo private and host a public repository with only test assets. We're okay with you perusing the code and using it to make something new, but we'd rather not make it _that_ easy to compile the game for yourself!

## Requirements
Currently, _Bo & Em: Space Rocks!_ is being developed in the Mono version of Godot 3.4.2. When Godot 4.0 releases, a migration _will_ be considered.

## Current Tasks
* The player controller for Bo is currently undergoing a massive overhaul - the old implementation was duct taped together and not expandable or easily tweakable. Not only will this refactor allow for better scalability of _Pawns_ as a whole, but it should also make fine-tuning the physics of the game notably easier.
* Once Bo is comfortable to control, the next course of action will be a functional dialogue system. Will it be over-engineered? Yes. Will it be packaged as a reusable Godot plugin? Ideally...

## Contributing
We will not accept any pull requests / issues / contributions of any kind from anyone who is _not_ a developer. This is solely an archival repository for everyone not on the team. Feel free to browse, though!
